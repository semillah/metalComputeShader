//
//  metalComputeShaderApp.swift
//  metalComputeShader
//
//  Created by Raul on 1/8/24.
//

import SwiftUI

@main
struct metalComputeShaderApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
