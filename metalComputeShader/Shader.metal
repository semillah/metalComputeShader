//
//  Shader.metal
//  metalComputeShader
//
//  Created by Raul on 1/8/24.
//

#include <metal_stdlib>  // Include the standard Metal library.
using namespace metal;  // Use the Metal namespace to simplify syntax.

// Define a kernel function 'basicComputeKernel'. Kernel functions are entry points for compute processing.
// This function will be called for each thread of the compute pass.
kernel void basicComputeKernel(
    device float *data [[ buffer(0) ]],  // Input: A pointer to a buffer of floats. The 'device' keyword indicates the memory is on the GPU.
    uint id [[ thread_position_in_grid ]])  // Input: The unique ID of the current thread within the entire grid.
{
    // The body of the compute kernel. This code is executed by each thread running the kernel.

    // For now, let's just increment each value in the data array.
    // 'id' corresponds to the unique position of the thread in the grid, effectively indexing into the data array.
    data[id] += 1.0;
    // This line takes the value in 'data' at the position specified by 'id' and increments it by 1.0.
    // Since each thread has a unique 'id', each one operates on a different part of the 'data' array.
}
