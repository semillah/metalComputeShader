//
//  ContentView.swift
//  metalComputeShader
//
//  Created by Raul on 1/8/24.
//

import SwiftUI
import Metal

// Define the ContentView structure which conforms to the View protocol.
struct ContentView: View {
    // The body property is required by the View protocol. It defines the view's content.
    var body: some View {
        VStack {
            // Create an image view with a system-provided globe icon.
            Image(systemName: "globe")
                .imageScale(.large)  // Set the image scale to large.
                .foregroundStyle(.tint)  // Apply the tint foreground style.
            // Create a text view displaying "Hello, world!".
            Text("Hello, world!")
                // Use the onAppear modifier to execute setupMetal() when the view appears.
                .onAppear {
                    setupMetal()
                }
        }
        .padding()  // Add padding around the VStack content.
    }
    
    // This function sets up the Metal environment and executes a basic compute shader.
    func setupMetal() {
        // 1. Get a device reference
        // Attempt to create a system default Metal device. This represents the GPU.
        guard let device = MTLCreateSystemDefaultDevice() else {
            fatalError("Device not found.")  // Terminate if no Metal device is found.
        }
        
        // 2. Create a command queue
        // Create a command queue on the device. This queue will manage command buffers.
        guard let commandQueue = device.makeCommandQueue() else {
            fatalError("Could not create command queue.")  // Terminate if the queue can't be created.
        }
        
        // 3. Load the compute shader from the default library
        // Access the default library containing Metal shader functions.
        let defaultLibrary = device.makeDefaultLibrary()!
        // Retrieve the 'basicComputeKernel' function from the library.
        let computeFunction = defaultLibrary.makeFunction(name: "basicComputeKernel")!
        
        // 4. Create a compute pipeline state
        // Create a pipeline state object for the compute function, which encapsulates compiled shaders.
        let computePipelineState = try! device.makeComputePipelineState(function: computeFunction)
        
        // 5. Create some dummy data to process
        // Initialize an array of 256 floating-point values set to 0.
        var data = [Float](repeating: 0, count: 256)
        // Calculate the size of the data array in bytes.
        let dataSize = data.count * MemoryLayout.size(ofValue: data[0])
        // Create a buffer on the GPU to store the data array.
        let dataBuffer = device.makeBuffer(bytes: &data, length: dataSize, options: [])
        
        // 6. Create a command buffer
        // Create a new command buffer to encode commands.
        let commandBuffer = commandQueue.makeCommandBuffer()!
        
        // 7. Create a command encoder
        // Create a compute command encoder to encode the compute commands.
        let computeCommandEncoder = commandBuffer.makeComputeCommandEncoder()!
        // Set the compute pipeline state for the encoder.
        computeCommandEncoder.setComputePipelineState(computePipelineState)
        // Set the data buffer as the input for the compute kernel at index 0.
        computeCommandEncoder.setBuffer(dataBuffer, offset: 0, index: 0)
        
        // 8. Dispatch the compute kernel
        // Define the number of threads per thread group (16 in this case).
        let threadsPerGroup = MTLSize(width: 16, height: 1, depth: 1)
        // Calculate the number of thread groups needed to process all data elements.
        let numThreadgroups = MTLSize(width: (data.count + 15) / 16, height: 1, depth: 1)
        // Dispatch the compute kernel with the defined thread groups and threads per group.
        computeCommandEncoder.dispatchThreadgroups(numThreadgroups, threadsPerThreadgroup: threadsPerGroup)
        
        // 9. Finalize encoding and commit the command buffer
        // End the encoding of commands.
        computeCommandEncoder.endEncoding()
        // Commit the command buffer to the command queue for execution.
        commandBuffer.commit()
        // Wait until all commands in the buffer are completed.
        commandBuffer.waitUntilCompleted()
        
        // 10. Read back the data
        // Check if the data buffer is successfully created.
        if let dataBuffer = dataBuffer {
            // Access the buffer's contents and cast it to an array of floats.
            let processedData = dataBuffer.contents().bindMemory(to: Float.self, capacity: data.count)
            // Iterate through the processed data and print each value.
            for i in 0 ..< data.count {
                print("Value at index \(i): \(processedData[i])")
            }
        } else {
            fatalError("Failed to create a data buffer.")  // Terminate if the data buffer is nil.
        }
    }
    
}
